import { NativeModules, DeviceEventEmitter } from 'react-native';


export default class SocketModule {

    SocketMod = NativeModules.Socket


    constructor (props) {
        // super(props)

        // this._dListeners = {};

        this.addDeviceListeners() 
    }

    _dListeners = {};


    addDeviceListeners (){
        if (Object.keys(this._dListeners).length){
            return this.emit('error', new Error("Socket listeners are already in place"))
        }
        this._dListeners.SocketSetted = DeviceEventEmitter.addListener('SocketSetted', params => {
            console.log(params)
        }); 
   
    }

    /**
     * Remove all event listeners and clean map
     */
    removeDeviceListeners () {
        Object.keys(this._dListeners).forEach(name => this._dListeners[name].remove())
        this._dListeners = {}
    }

    setupSocket(address, port) {
        NativeModules.Socket.setupSocket(address, port);
    }

    startThread() {
        NativeModules.Socket.startThread();
    }

    setMessage(message) {
        NativeModules.Socket.setMessage(message);
    }

}
