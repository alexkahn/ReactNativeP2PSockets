import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.Iterator;
import java.util.Set;

/*
from: www.java2s.com
adapted by Alexandre Kahn
*/

public class ServerSocket implements Runnable{

    private int port;

    public ServerSocket(int port){
        this.port = port;
    }

  public static void processReadySet(Set readySet) throws Exception {
    Iterator iterator = readySet.iterator();
    while (iterator.hasNext()) {
      SelectionKey key = (SelectionKey) iterator.next();
      iterator.remove();
      if (key.isAcceptable()) {
        ServerSocketChannel ssChannel = (ServerSocketChannel) key.channel();
        SocketChannel sChannel = (SocketChannel) ssChannel.accept();
        sChannel.configureBlocking(false);
        sChannel.register(key.selector(), SelectionKey.OP_READ);
      }
      if (key.isReadable()) {
        String msg = processRead(key);
        System.out.println(msg);

        // Send msg to React Native here
      }
    }
  }
  public static String processRead(SelectionKey key) throws Exception {
    SocketChannel sChannel = (SocketChannel) key.channel();
    ByteBuffer buffer = ByteBuffer.allocate(1024);
    int bytesCount = sChannel.read(buffer);
    if (bytesCount > 0) {
      buffer.flip();
      return new String(buffer.array());
    }
    return "NoMessage";
  }

    @Override
    public void run() {
      try {
          InetAddress hostIPAddress = InetAddress.getByName("192.168.3.100");
//          InetAddress hostIPAddress = InetAddress.getLocalHost();
          Selector selector = Selector.open();
          ServerSocketChannel ssChannel = ServerSocketChannel.open();
          ssChannel.configureBlocking(false);
          ssChannel.socket().bind(new InetSocketAddress(hostIPAddress, port));
          ssChannel.register(selector, SelectionKey.OP_ACCEPT);
          while (true) {
              if (selector.select() <= 0) {
                  continue;
              }
              processReadySet(selector.selectedKeys());
          }
      }
      catch (IOException e) {
          e.printStackTrace();
      } catch (Exception e) {
          e.printStackTrace();
      }
    }
}