import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.SocketChannel;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.util.Iterator;
import java.util.Set;

/*
from: www.java2s.com
adapted by Alexandre Kahn
*/

public class Socket  {

    private InetAddress serverIPAddress;
    private int port;
    public static String message = "This is the msg";

    public void setMessage(String message) {
        this.message = message;
    }

    public static boolean processReadySet(Set readySet) throws Exception {
        Iterator iterator = readySet.iterator();
        while (iterator.hasNext()) {
            SelectionKey key = (SelectionKey)
                    iterator.next();
            iterator.remove();
            if (key.isConnectable()) {
                boolean connected = processConnect(key);
                if (!connected) {
                    return true; // Exit
                }
            }

            if (key.isWritable()) {
                String msg = message;
                message = "";

                SocketChannel sChannel = (SocketChannel) key.channel();
                ByteBuffer buffer = ByteBuffer.wrap(msg.getBytes());
                sChannel.write(buffer);

            }
        }
        return false; // Not done yet
    }

    public static boolean processConnect(SelectionKey key) throws Exception{
        SocketChannel channel = (SocketChannel) key.channel();
        while (channel.isConnectionPending()) {
            channel.finishConnect();
        }
        return true;
    }

    public Socket(String address, int port) {
        try {
            this.serverIPAddress = InetAddress.getByName(address);
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
        this.port = port;
    }

    public void startThread(){
        SocketThread socketThread = new SocketThread();
        Thread thread = new Thread(socketThread);
        thread.start();
    }

    public class SocketThread implements Runnable {

        @Override
        public void run() {
            try {
                InetSocketAddress serverAddress = new InetSocketAddress(serverIPAddress, port);
                Selector selector = Selector.open();
                SocketChannel channel = SocketChannel.open();
                channel.configureBlocking(false);
                channel.connect(serverAddress);
                int operations = SelectionKey.OP_CONNECT | SelectionKey.OP_READ
                        | SelectionKey.OP_WRITE;
                channel.register(selector, operations);
                while (true) {
                    if (selector.select() > 0) {
                        boolean doneStatus = processReadySet(selector.selectedKeys());
                        if (doneStatus) {
                            break;
                        }
                    }
                }
                channel.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
