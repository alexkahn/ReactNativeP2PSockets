public class main {
    public static void main(String[] args) throws Exception {

        int port = 19200;

        ServerSocket serverSocket = new ServerSocket(port);
        Thread serverThread = new Thread(serverSocket);
        serverThread.start();

        Socket mainsocket = new Socket("192.168.3.100", port);
//        Thread mainsocketThread = new Thread(mainsocket);
//        mainsocketThread.start();
        mainsocket.startThread();

        int i = 0;
        while(true) {
            mainsocket.setMessage("Hello + " + ++i);
            System.out.println(i);
            Thread.sleep(5000);
        }

    }
}
