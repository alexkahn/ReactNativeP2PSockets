package com.reactnativesockets;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.Iterator;
import java.util.Set;

import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.NativeModule;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.modules.core.DeviceEventManagerModule;
import android.support.annotation.Nullable;

/*
from: www.java2s.com
adapted by Alexandre Kahn
*/

public class ServerSocket extends ReactContextBaseJavaModule  {

    private int port;
    private InetAddress hostIPAddress;
 
    static ReactApplicationContext reactContext;

    public ServerSocket(ReactApplicationContext reactContext){
        super(reactContext);
        this.reactContext = reactContext;
    }

    @ReactMethod
    public void setupServerSocket(String address, int port) {
        try{
            this.hostIPAddress = InetAddress.getByName(address);
            this.port = port;
        }
        catch (UnknownHostException e) {
            e.printStackTrace();
        }
    }

    // Parts for communicating with the React Native part
    @Override
    public String getName() {
        return "ServerSocket";
    }

    protected static void sendEvent(ReactContext reactContext,
                             String eventName,
                             @Nullable WritableMap params) {
        reactContext
                .getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class)
                .emit(eventName, params);
    }

  public static void processReadySet(Set readySet) throws Exception {
    Iterator iterator = readySet.iterator();
    while (iterator.hasNext()) {
      SelectionKey key = (SelectionKey) iterator.next();
      iterator.remove();
      if (key.isAcceptable()) {
        ServerSocketChannel ssChannel = (ServerSocketChannel) key.channel();
        SocketChannel sChannel = (SocketChannel) ssChannel.accept();
        sChannel.configureBlocking(false);
        sChannel.register(key.selector(), SelectionKey.OP_READ);
      }
      if (key.isReadable()) {
        String msg = processRead(key);
        // System.out.println(msg);

        // Send msg to React Native
         WritableMap params = Arguments.createMap();
         params.putString("message", msg);
         sendEvent(reactContext, "ReceivedMessage", params);
      }
    }
  }
  public static String processRead(SelectionKey key) throws Exception {
    SocketChannel sChannel = (SocketChannel) key.channel();
    ByteBuffer buffer = ByteBuffer.allocate(1024);
    int bytesCount = sChannel.read(buffer);
    if (bytesCount > 0) {
      buffer.flip();
      return new String(buffer.array());
    }
    return "NoMessage";
  }

  @ReactMethod
    public void startThread(){
        ServerSocketThread serverSocketThread = new ServerSocketThread();
        Thread thread = new Thread(serverSocketThread);
        thread.start();
        WritableMap params = Arguments.createMap();
        params.putString("message", "True");
        sendEvent(reactContext, "ThreadStarted", params);
    }

    public class ServerSocketThread implements Runnable {
        @Override
        public void run() {
        try {
            Selector selector = Selector.open();
            ServerSocketChannel ssChannel = ServerSocketChannel.open();
            ssChannel.configureBlocking(false);
            ssChannel.socket().bind(new InetSocketAddress(hostIPAddress, port));
            ssChannel.register(selector, SelectionKey.OP_ACCEPT);
            while (true) {
                if (selector.select() <= 0) {
                    continue;
                }
                processReadySet(selector.selectedKeys());
            }
        }
        catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        }
    }
}