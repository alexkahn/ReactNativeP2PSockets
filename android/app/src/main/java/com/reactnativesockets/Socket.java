package com.reactnativesockets;

import java.io.IOException;
import java.net.UnknownHostException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.Iterator;
import java.util.Set;

import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.NativeModule;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.modules.core.DeviceEventManagerModule;
import android.support.annotation.Nullable;

/*
from: www.java2s.com
adapted by Alexandre Kahn
*/

public class Socket extends ReactContextBaseJavaModule  {

    static ReactApplicationContext reactContext;

    public Socket(ReactApplicationContext reactContext){
        super(reactContext);
        this.reactContext = reactContext;
    }


    // Parts for communicating with the React Native part
    @Override
    public String getName() {
        return "Socket";
    }

    protected static void sendEvent(ReactContext reactContext,
                             String eventName,
                             @Nullable WritableMap params) {
        reactContext
                .getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class)
                .emit(eventName, params);
    }

    private InetAddress serverIPAddress;
    private int port;
    public static String message = "This is the msg";
    
    @ReactMethod
    public void setMessage(String msg) {
        message = msg;

        WritableMap params = Arguments.createMap();
        params.putString("message", "The message is:" + message);
        sendEvent(reactContext, "MessageSetted", params);

    }

    public static boolean processReadySet(Set readySet) throws Exception {
        Iterator iterator = readySet.iterator();
        while (iterator.hasNext()) {
            SelectionKey key = (SelectionKey)
                    iterator.next();
            iterator.remove();
            if (key.isConnectable()) {
                boolean connected = processConnect(key);
                if (!connected) {
                    return true; // Exit
                }
            }

            if (key.isWritable()) {
                String msg = message;
                message = "";

                SocketChannel sChannel = (SocketChannel) key.channel();
                ByteBuffer buffer = ByteBuffer.wrap(msg.getBytes());
                sChannel.write(buffer);

            }
        }
        return false; // Not done yet
    }

    public static boolean processConnect(SelectionKey key) throws Exception{
        SocketChannel channel = (SocketChannel) key.channel();
        while (channel.isConnectionPending()) {
            channel.finishConnect();
        }
        return true;
    }

    @ReactMethod
    public void setupSocket(String address, int port) {
        WritableMap params = Arguments.createMap();
        
        try {
            this.serverIPAddress = InetAddress.getByName(address);
        } catch (UnknownHostException e) {
            e.printStackTrace();
            params.putString("error", e.toString());
            sendEvent(reactContext, "SocketSetted", params);
        }
        this.port = port;
        params.putString("msg", "True");
        sendEvent(reactContext, "SocketSetted", params);
    }

    @ReactMethod
    public void startThread(){
        SocketThread socketThread = new SocketThread();
        Thread thread = new Thread(socketThread);
        thread.start();
        WritableMap params = Arguments.createMap();
        params.putString("message", "True");
        sendEvent(reactContext, "ThreadStarted", params);
    }

    public class SocketThread implements Runnable {
        @Override
        public void run() {
            try {
                InetSocketAddress serverAddress = new InetSocketAddress(
                        serverIPAddress, port);
                Selector selector = Selector.open();
                SocketChannel channel = SocketChannel.open();
                channel.configureBlocking(false);
                channel.connect(serverAddress);
                int operations = SelectionKey.OP_CONNECT | SelectionKey.OP_READ
                    |   SelectionKey.OP_WRITE;
                channel.register(selector, operations);
                while (true) {
                    if (selector.select() > 0) {
                        boolean doneStatus = processReadySet(selector.selectedKeys());
                        if (doneStatus) {
                            break;
                        }
                    }
                }
                channel.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
